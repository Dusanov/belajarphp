<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="UTF-8">
	<title>Belajar PHP</title>
</head>
<body>

<h2> Daftar Nama Peserta </h2>

<!-- membuat table pada html-->
<table border="1" cellspacing="0" cellpadding="4">
	<tr><th> N0. </th><th> Nama Peserta </th></tr>

<?php
	//double quoted ""
	$situs = "www.duniailkom.com";
	$belajar = "Sedang belajar programming di $situs";
	echo $belajar;
	echo "<br>";
	
	// type data Boolean
	$benar = True;
	if ($benar) {
		echo "Benar";
	}
	echo "<br>";
	
	// type data Array
	$siswa = ["Andri","Joko","Sukma","Rina","Sari"];
	
	// menambahkan element array
	$siswa[5] = "Dhendy";
	$siswa[6] = "Dennis";
	echo $siswa[6];
	echo "<br>";
	
	// menghapus element array
	unset ($siswa[5]);

	//array di dalam array
	//		  0		0		  1		1	0		   1	
	$pegawai =[["Dusanov","Aprila"],["Christie","Mama"]];
	echo $pegawai[1][1];
	echo "<br>";

	//Associative array (array yang indexnya bukan penomoran, tetapi dengan objek lain seperti string)
	$karyawan = [
				 "satu" 	=> "Budi",
				 "dua" 		=> "Andi",
				 "tiga"		=> "Rita",
				 "empat"	=> "Wati"
				];

	echo $karyawan["dua"];
	echo "<br>";

	$kelas = [
				"kelas_x" 	=> ["Anwar","Yanti","Reza"],
				"kelas_xi"	=> ["Diki","Anto","Susan"],
			];
	echo $kelas["kelas_xi"][1];
	echo "<br>";

	// tipe data object
	class Pelajar {
		public $nama;
		public $umur;
		public $tgl_lahir;
	
		public function get_umur() {
			return $this->umur;
		}	
	}

	$anandi = new Pelajar;
	$anandi->nama = "Anandi";
	$anandi->umur = "17";
	$anandi->tgl_lahir ="13 Des 1990";

	echo "<pre>";
	print_r($anandi);
	echo "</pre>";
	echo "<br>";

	// konversi tipe data (mengubah tipe data string ke integer dan integer ke string)
	$string_angka = "99 ekor kucing";
	$string_kata  = "belajar PHP";
	$angka_int	  = 12;
	
	$string_kata = (int) $string_kata;
	echo $string_kata;
	echo "<br>";
	
	// seluruh angka tetap, namun bertipe string
	$angka_int = (string) $angka_int;
	echo $angka_int;
	
	// kode php untuk menampilkan list
	for($i=1;$i<=10;$i++) {
		echo "<tr><td> $i </td><td> Nama Peserta $i</td></tr>";
	}

	echo"<br>";

	///// Soal POINT B. PT Fiyaris Prima Teknologi ///////

	for($i=1; $i<=15; $i+=2) {
		echo $i;
	}

	echo"<br>";

	echo date('n-y-d', strtotime('2020-11-23'));

	echo"<br>";

	$tanggal = "2020-11-23";
	$date1= date('n-y-d', strtotime('+6 month', strtotime($tanggal)));
	echo $date1;

	echo "<br>";

	$tgl 		= new DateTime('2020-11-23');
	$sekarang	= new DateTime('2020-2-11');
	$year			= $sekarang->diff($tgl)->y;
	$month			= $sekarang->diff($tgl)->m;
	$day			= $sekarang->diff($tgl)->d;
	echo $year . "tahun". $month . "bulan" . $day . "hari";

	echo"<br>";

	$bilangan = ["1" => "5", "2" => "10", "3" => "15", "4" => "20", "5" => "30", "6" => "100"];
	asort($bilangan);
	echo '<pre>'; print_r($bilangan);

	echo"<br>";

	$angka = ["1" => "5", "2" => "10", "3" => "15", "4" => "20", "5" => "30", "6" => "100", 
	"7" => "120"];
	asort($angka);
	echo '<pre>'; print_r($angka);

	echo"<br>";

	$bilang = ["1" => "5", "2" => "", "3" => "15", "4" => "20", "5" => "30", "6" => "100"];
	ksort($bilang);
	echo '<pre>'; print_r($bilang);

	echo"<br>";

	$kata = "Aku Suka Makan Nasi Padang";
	echo substr($kata,0,3);
	echo "<br>";
	echo substr($kata,0,8);
	echo "<br>";
	echo substr($kata,0,14);
	echo "<br>";
	echo substr($kata,0,19);
	echo "<br>";
	echo substr($kata,0,27);
	echo "<br>";
	echo substr($kata,-6);
	echo "<br>";
	echo substr($kata,-11);
	echo "<br>";
	echo substr($kata,-17);
	echo "<br>";
	echo substr($kata,-22);
	echo "<br>";
	echo substr($kata,-26);
	echo "<br>";
	
?>
</table>
</body>
</html>